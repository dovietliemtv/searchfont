import Document, { Head, Main, NextScript } from 'next/document';
export default class MyDocument extends Document {
  render() {
    return (
      <html>
            <Head>
                <meta charSet='utf-8' />
                <meta name="description" content="Researh font" />
                <meta name='viewport' content='initial-scale=1.0, width=device-width' />
                <title>Researh font</title>
                <link rel="stylesheet" href="/_next/static/style.css"/>
            </Head>
            <body>
                <Main />
                <NextScript />
            </body>
      </html>
    )
  }
}