import React from 'react';
import InputButton from '../components/inputButton';
import Result from '../components/result';
import '../scss/styles.scss';
import Loading from '../components/loading';

class Index extends React.Component{
    display = false;
    constructor(props){
        super();
        this.state = {
            info : {
                postscriptName : null,
                fullName : null,
                familyName : null,
                copyright : null,
                version : null,
                ascent : null,
                descent : null,
                lineGap : null,
                underlineThickness : null,
                italicAngle : null,
                xHeight : null,
                postscriptName : null,
                postscriptName : null,
            },
            display: false,
            error: null,
            loading: false
        }
    }

    clickInput = uploadFile =>{
        const axios = require('axios');
        let self = this;
        let bodyFormData = new FormData();
        bodyFormData.append ('font', uploadFile);
        this.setState({ loading: true }, () => {
            axios.post(
                'https://safe-mountain-27435.herokuapp.com/getFontInfo',
                bodyFormData,
                { headers: {'Content-Type': 'multipart/form-data' }}
            ).then(function (response) {
                //handle success
                self.setState({
                    info: response.data,
                    display: true,
                    loading: false
                });
            }).catch(function (response) {
                //handle error
                alert('Error: Cant find this file!');
                self.setState({
                    loading: false
                });
            });
        });
        
            
    }
    render(){
        const loading = this.state.loading;
        return(
            <section className={'sf-s-search'}>
                <div className={'sf-b-container'}>
                    <h4>Font Info</h4>
                    <InputButton clickInputButton={this.clickInput}/>
                    {loading ? <Loading /> :  <Result display={this.state.display} data={this.state}/>}
                </div>
            </section>
        )
    }
}
export default Index;