import React from 'react';
export default class Result extends React.Component{
    render(){
        return(
            <div className={`sf-b-result ${this.props.display ? 'show' : 'hide'}`} >
               <div className={'sf-b-result-inner'}>
                    <p>Postscript name: <span>{this.props.data.info.postscriptName}</span></p>
                    <p>Full name: <span>{this.props.data.info.fullName}</span></p>
                    <p>Family name: <span>{this.props.data.info.familyName}</span></p>
                    <p>Copyright:</p>
                    <p>{this.props.data.info.copyright}</p>
                    <p>Version:  <span>{this.props.data.info.version}</span></p>
                    <p>Ascent:  <span>{this.props.data.info.ascent}</span></p>
                    <p>Descent: <span> {this.props.data.info.descent}</span></p>
                    <p>Line gap:   <span> {this.props.data.info.lineGap}</span></p>
                    <p>Underline thickness: <span>{this.props.data.info.underlineThickness}</span></p>
                    <p>Italic angle: <span>{this.props.data.info.italicAngle}</span></p>
                    <p>xHeight: <span>{this.props.data.info.xHeight}</span></p>
               </div>
            </div>
        )
    }
}