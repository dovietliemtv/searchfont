import React from 'react';
export default class InputButton extends React.Component{
    handleChange = () => {
        this.handleChange = this.handleChange.bind(this);
        this.props.clickInputButton(this.fileUpload.files[0]);
    }
    render(){
        return(
            <div className='file-upload'>
                <label htmlFor="upload" className="file-upload__label">Drop your font here!</label>
                <input id="upload" 
                className="file-upload__input" 
                type="file"
                accept=".ttf,.otf,.fnt,.WOFF, .EOT,.ABF,.ACFM,.AFM,.AMFM,.BDF,.CHA,.CHR,.COMPOSITEFONT,
                .DFONT,.ETX,.FNT,.FON,.FOT,.GDR,.GXF,.MF,.ODTTF,.PCF,.PFA,.PFB,.PFM,.PMT,.SFD,.SFP,.SUIT,.TTC,
                .TTF,.VFB,.VLW,.VNF,.WOFF2,.XFN,.XFT"
                onChange={ this.handleChange } 
                ref={(ref) => this.fileUpload = ref}
                />
            </div>           
        )
    }
}