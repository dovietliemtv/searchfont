import React from 'react';
export default class Loading extends React.Component{
    render(){
        return(
            <div className='sf-s-loadding'>
                <div className='sf-loadding-inner'>
                    <img src="/static/loading.gif" alt="loading" />
                </div>
            </div>           
        )
    }
}