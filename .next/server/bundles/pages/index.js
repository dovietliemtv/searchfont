module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),
/* 1 */,
/* 2 */,
/* 3 */,
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(5);


/***/ }),
/* 5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });

// EXTERNAL MODULE: external "react"
var external__react_ = __webpack_require__(0);
var external__react__default = /*#__PURE__*/__webpack_require__.n(external__react_);

// CONCATENATED MODULE: ./components/inputButton.js
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }



var inputButton_InputButton =
/*#__PURE__*/
function (_React$Component) {
  _inherits(InputButton, _React$Component);

  function InputButton() {
    var _ref;

    var _temp, _this;

    _classCallCheck(this, InputButton);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _possibleConstructorReturn(_this, (_temp = _this = _possibleConstructorReturn(this, (_ref = InputButton.__proto__ || Object.getPrototypeOf(InputButton)).call.apply(_ref, [this].concat(args))), Object.defineProperty(_assertThisInitialized(_this), "handleChange", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        _this.handleChange = _this.handleChange.bind(_assertThisInitialized(_this));

        _this.props.clickInputButton(_this.fileUpload.files[0]);
      }
    }), _temp));
  }

  _createClass(InputButton, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      return external__react__default.a.createElement("div", {
        className: "file-upload"
      }, external__react__default.a.createElement("label", {
        htmlFor: "upload",
        className: "file-upload__label"
      }, "Drop your font here!"), external__react__default.a.createElement("input", {
        id: "upload",
        className: "file-upload__input",
        type: "file",
        accept: ".ttf,.otf,.fnt,.WOFF, .EOT,.ABF,.ACFM,.AFM,.AMFM,.BDF,.CHA,.CHR,.COMPOSITEFONT,\r .DFONT,.ETX,.FNT,.FON,.FOT,.GDR,.GXF,.MF,.ODTTF,.PCF,.PFA,.PFB,.PFM,.PMT,.SFD,.SFP,.SUIT,.TTC,\r .TTF,.VFB,.VLW,.VNF,.WOFF2,.XFN,.XFT",
        onChange: this.handleChange,
        ref: function ref(_ref2) {
          return _this2.fileUpload = _ref2;
        }
      }));
    }
  }]);

  return InputButton;
}(external__react__default.a.Component);


// CONCATENATED MODULE: ./components/result.js
function result__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { result__typeof = function _typeof(obj) { return typeof obj; }; } else { result__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return result__typeof(obj); }

function result__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function result__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function result__createClass(Constructor, protoProps, staticProps) { if (protoProps) result__defineProperties(Constructor.prototype, protoProps); if (staticProps) result__defineProperties(Constructor, staticProps); return Constructor; }

function result__possibleConstructorReturn(self, call) { if (call && (result__typeof(call) === "object" || typeof call === "function")) { return call; } return result__assertThisInitialized(self); }

function result__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function result__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }



var result_Result =
/*#__PURE__*/
function (_React$Component) {
  result__inherits(Result, _React$Component);

  function Result() {
    result__classCallCheck(this, Result);

    return result__possibleConstructorReturn(this, (Result.__proto__ || Object.getPrototypeOf(Result)).apply(this, arguments));
  }

  result__createClass(Result, [{
    key: "render",
    value: function render() {
      return external__react__default.a.createElement("div", {
        className: "sf-b-result ".concat(this.props.display ? 'show' : 'hide')
      }, external__react__default.a.createElement("div", {
        className: 'sf-b-result-inner'
      }, external__react__default.a.createElement("p", null, "Postscript name: ", external__react__default.a.createElement("span", null, this.props.data.info.postscriptName)), external__react__default.a.createElement("p", null, "Full name: ", external__react__default.a.createElement("span", null, this.props.data.info.fullName)), external__react__default.a.createElement("p", null, "Family name: ", external__react__default.a.createElement("span", null, this.props.data.info.familyName)), external__react__default.a.createElement("p", null, "Copyright:"), external__react__default.a.createElement("p", null, this.props.data.info.copyright), external__react__default.a.createElement("p", null, "Version:  ", external__react__default.a.createElement("span", null, this.props.data.info.version)), external__react__default.a.createElement("p", null, "Ascent:  ", external__react__default.a.createElement("span", null, this.props.data.info.ascent)), external__react__default.a.createElement("p", null, "Descent: ", external__react__default.a.createElement("span", null, " ", this.props.data.info.descent)), external__react__default.a.createElement("p", null, "Line gap:   ", external__react__default.a.createElement("span", null, " ", this.props.data.info.lineGap)), external__react__default.a.createElement("p", null, "Underline thickness: ", external__react__default.a.createElement("span", null, this.props.data.info.underlineThickness)), external__react__default.a.createElement("p", null, "Italic angle: ", external__react__default.a.createElement("span", null, this.props.data.info.italicAngle)), external__react__default.a.createElement("p", null, "xHeight: ", external__react__default.a.createElement("span", null, this.props.data.info.xHeight))));
    }
  }]);

  return Result;
}(external__react__default.a.Component);


// EXTERNAL MODULE: ./scss/styles.scss
var styles = __webpack_require__(6);
var styles_default = /*#__PURE__*/__webpack_require__.n(styles);

// CONCATENATED MODULE: ./components/loading.js
function loading__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { loading__typeof = function _typeof(obj) { return typeof obj; }; } else { loading__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return loading__typeof(obj); }

function loading__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function loading__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function loading__createClass(Constructor, protoProps, staticProps) { if (protoProps) loading__defineProperties(Constructor.prototype, protoProps); if (staticProps) loading__defineProperties(Constructor, staticProps); return Constructor; }

function loading__possibleConstructorReturn(self, call) { if (call && (loading__typeof(call) === "object" || typeof call === "function")) { return call; } return loading__assertThisInitialized(self); }

function loading__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function loading__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }



var loading_Loading =
/*#__PURE__*/
function (_React$Component) {
  loading__inherits(Loading, _React$Component);

  function Loading() {
    loading__classCallCheck(this, Loading);

    return loading__possibleConstructorReturn(this, (Loading.__proto__ || Object.getPrototypeOf(Loading)).apply(this, arguments));
  }

  loading__createClass(Loading, [{
    key: "render",
    value: function render() {
      return external__react__default.a.createElement("div", {
        className: "sf-s-loadding"
      }, external__react__default.a.createElement("div", {
        className: "sf-loadding-inner"
      }, external__react__default.a.createElement("img", {
        src: "/static/loading.gif",
        alt: "loading"
      })));
    }
  }]);

  return Loading;
}(external__react__default.a.Component);


// CONCATENATED MODULE: ./pages/index.js
function pages__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { pages__typeof = function _typeof(obj) { return typeof obj; }; } else { pages__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return pages__typeof(obj); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function pages__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function pages__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function pages__createClass(Constructor, protoProps, staticProps) { if (protoProps) pages__defineProperties(Constructor.prototype, protoProps); if (staticProps) pages__defineProperties(Constructor, staticProps); return Constructor; }

function pages__possibleConstructorReturn(self, call) { if (call && (pages__typeof(call) === "object" || typeof call === "function")) { return call; } return pages__assertThisInitialized(self); }

function pages__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function pages__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }







var pages_Index =
/*#__PURE__*/
function (_React$Component) {
  pages__inherits(Index, _React$Component);

  function Index(props) {
    var _info;

    var _this;

    pages__classCallCheck(this, Index);

    _this = pages__possibleConstructorReturn(this, (Index.__proto__ || Object.getPrototypeOf(Index)).call(this));
    Object.defineProperty(pages__assertThisInitialized(_this), "display", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: false
    });
    Object.defineProperty(pages__assertThisInitialized(_this), "clickInput", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value(uploadFile) {
        var axios = __webpack_require__(7);

        var self = pages__assertThisInitialized(_this);

        var bodyFormData = new FormData();
        bodyFormData.append('font', uploadFile);

        _this.setState({
          loading: true
        }, function () {
          axios.post('https://safe-mountain-27435.herokuapp.com/getFontInfo', bodyFormData, {
            headers: {
              'Content-Type': 'multipart/form-data'
            }
          }).then(function (response) {
            //handle success
            self.setState({
              info: response.data,
              display: true,
              loading: false
            });
          }).catch(function (response) {
            //handle error
            alert('Error: Cant find this file!');
            self.setState({
              loading: false
            });
          });
        });
      }
    });
    _this.state = {
      info: (_info = {
        postscriptName: null,
        fullName: null,
        familyName: null,
        copyright: null,
        version: null,
        ascent: null,
        descent: null,
        lineGap: null,
        underlineThickness: null,
        italicAngle: null,
        xHeight: null
      }, _defineProperty(_info, "postscriptName", null), _defineProperty(_info, "postscriptName", null), _info),
      display: false,
      error: null,
      loading: false
    };
    return _this;
  }

  pages__createClass(Index, [{
    key: "render",
    value: function render() {
      var loading = this.state.loading;
      return external__react__default.a.createElement("section", {
        className: 'sf-s-search'
      }, external__react__default.a.createElement("div", {
        className: 'sf-b-container'
      }, external__react__default.a.createElement("h4", null, "Font Info"), external__react__default.a.createElement(inputButton_InputButton, {
        clickInputButton: this.clickInput
      }), loading ? external__react__default.a.createElement(loading_Loading, null) : external__react__default.a.createElement(result_Result, {
        display: this.state.display,
        data: this.state
      })));
    }
  }]);

  return Index;
}(external__react__default.a.Component);

/* harmony default export */ var pages = __webpack_exports__["default"] = (pages_Index);

/***/ }),
/* 6 */
/***/ (function(module, exports) {



/***/ }),
/* 7 */
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ })
/******/ ]);