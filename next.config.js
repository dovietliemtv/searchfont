const withSass = require('@zeit/next-sass')
const withPlugins = require('next-compose-plugins');
const sassConfig = {
    cssModules: true,
    cssLoaderOptions: {
      importLoaders: 1,
      localIdentName: '[local]___[name]___[hash:base64:5]'
    }
  };
module.exports = withPlugins([
  [withSass],[sassConfig]
])